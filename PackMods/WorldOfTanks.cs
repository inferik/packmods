﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Dynamic;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;

namespace PackMods
{
    public class WorldOfTanks
    {
        public static bool CheckPath(string path = "")
        {
            if(String.IsNullOrEmpty(path))
            {
                return false;
            }

            var check = new string[]
            {
                "WorldOfTanks.exe",
                "WoTLauncher.exe",
                "res_mods",
                "res",
                "paths.xml",
            };
            string link;
            foreach(var el in check)
            {
                link = path + "\\" + el;
                if (!Directory.Exists(link) && !File.Exists(link))
                {
                    return false;
                }
            }
            return true;
        }


        private static dynamic _getExpandoFromXml(String file, XElement node = null)
        {
            if (String.IsNullOrWhiteSpace(file) && node == null) return null;

            // If a file is not empty then load the xml and overwrite node with the
            // root element of the loaded document
            node = !String.IsNullOrWhiteSpace(file) ? XDocument.Load(file).Root : node;

            IDictionary<String, dynamic> result = new ExpandoObject();

            // implement fix as suggested by [ndinges]
            var pluralizationService =
                PluralizationService.CreateService(CultureInfo.CreateSpecificCulture("en-us"));

            // use parallel as we dont really care of the order of our properties
            node.Elements().AsParallel().ForAll(gn =>
            {
                // Determine if node is a collection container
                var isCollection = gn.HasElements &&
                    (
                        // if multiple child elements and all the node names are the same
                        gn.Elements().Count() > 1 &&
                        gn.Elements().All(
                            e => e.Name.LocalName.ToLower() == gn.Elements().First().Name.LocalName) ||

                        // if there's only one child element then determine using the PluralizationService if
                        // the pluralization of the child elements name matches the parent node. 
                        gn.Name.LocalName.ToLower() == pluralizationService.Pluralize(
                            gn.Elements().First().Name.LocalName).ToLower()
                    );

                // If the current node is a container node then we want to skip adding
                // the container node itself, but instead we load the children elements
                // of the current node. If the current node has child elements then load
                // those child elements recursively
                var items = isCollection ? gn.Elements().ToList() : new List<XElement>() { gn };

                var values = new List<dynamic>();

                // use parallel as we dont really care of the order of our properties
                // and it will help processing larger XMLs
                items.AsParallel().ForAll(i => values.Add((i.HasElements) ?
                   _getExpandoFromXml(null, i) : i.Value.Trim()));

                // Add the object name + value or value collection to the dictionary
                result[gn.Name.LocalName] = isCollection ? values : values.FirstOrDefault();
            });
            return result;
        }
    }
}

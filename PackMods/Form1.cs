﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PackMods
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            if(!Properties.Settings.Default.licenseAccept)
            {
                license();
            }
            InitializeComponent();

            
        }

        private void appTitle_Click(object sender, EventArgs e)
        {
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            if (!LicenseAccept.result)
            {
                this.Close();
            }

            initWotPath();

            loadValues();
        }

        private void loadValues()
        {
            gamePathTextBox.Text = Properties.Settings.Default.wotPath;
        }

        private void gamePathButton_Click(object sender, EventArgs e)
        {
            setWotDirectory(false);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            license(true);
        }
        

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://inferik.site/donate/packmods");
        }

        #region NotActions
        private void initWotPath(string wotPath = "")
        {
            // If user argument is not exists or empty
            if (!WorldOfTanks.CheckPath(wotPath))
            {
                wotPath = Properties.Settings.Default.wotPath;
            }
            else
            {
                return;
            }

            // If valid directory in settings
            if (WorldOfTanks.CheckPath(wotPath))
            {
                return;
            }

            // Checking application current directory
            if(WorldOfTanks.CheckPath(AppDomain.CurrentDomain.BaseDirectory))
            {
                Properties.Settings.Default.wotPath = AppDomain.CurrentDomain.BaseDirectory;
                return;
            }

            setWotDirectory(true);
        }

        private void setWotDirectory(bool init = false)
        {
            string selectDirectory;
            DialogResult result;
            {
                result = wotFolderBrowser.ShowDialog();
                selectDirectory = wotFolderBrowser.SelectedPath;
            } while (init && result != DialogResult.OK && !WorldOfTanks.CheckPath(selectDirectory)) ;

        }

        private void license(bool viewing = false)
        {
            LicenseAccept la = new LicenseAccept();
            la.viewing = viewing;
            la.ShowDialog();
        }

        #endregion
    }
}

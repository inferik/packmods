﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PackMods
{
    public partial class LicenseAccept : Form
    {
        public bool viewing = false;

        public static bool result {
            get
            {
                return Properties.Settings.Default.licenseAccept;
            }
            set
            {
                Properties.Settings.Default.licenseAccept = value;
            }
        }

        public LicenseAccept()
        {
            InitializeComponent();
            this.decline.Visible = this.viewing;
        }

        private void LicenseAccept_Load(object sender, EventArgs e)
        {
        }

        private void accept_Click(object sender, EventArgs e)
        {
            LicenseAccept.result = true;
            this.Close();
        }

        private void decline_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://inferik.site/donate/packmods");
        }
    }
}
